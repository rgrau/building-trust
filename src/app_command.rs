extern crate md5;
use std::collections::HashMap;

#[derive(Serialize, Deserialize, Debug)]
pub struct AppCommand {
    pub command: Vec<String>,
    pub cwd: Option<String>,
    pub state: String,
}

#[derive(Debug)]
pub struct StartCommand {
    pub command: Vec<String>,
    pub cwd: String
}

pub struct StopCommand {
    pub command: Vec<String>
}

pub fn key_for(c: &AppCommand) -> String {
    format!("{:x}",
            md5::compute(format!("{}", &c.command.join("|"))))
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn create_command () {
        let c = AppCommand{ command: vec!["A".to_string()], cwd: "123".to_string(), state: "hola".to_string()};
        assert!(
            match c {AppCommand => true}
        );
    }}
