#![feature(plugin)]
#![plugin(rocket_codegen)]

use std::thread;
use rocket::State;
use rocket::response::content;

use std::collections::HashMap;
use std::sync::mpsc;


mod app_command;
use app_command::*;
// use rocket::response::content::Json;
extern crate rocket;
extern crate serde_json;
#[macro_use] extern crate rocket_contrib;
#[macro_use] extern crate serde_derive;

use rocket_contrib::Json;

#[derive(Debug)]
struct RunningCommands {
    commands: HashMap<String, AppCommand>,
}


extern crate tokio_core;
extern crate tokio_process;

use std::process::Command;

use tokio_core::reactor::Core;
use tokio_process::CommandExt;

use std::path::Path;

fn appup(c: &StartCommand) {
    // Create our own local event loop
    loop {
        let mut core = Core::new().unwrap();

        // Use the standard library's `Command` type to build a process and
        // then execute it via the `CommandExt` trait.
        let mut new_c = Command::new(&c.command[0]);

        for i in &c.command[1..] {
            new_c.arg(i);
        }

        let child = new_c
            .current_dir(&Path::new(&c.cwd))
            .spawn_async(&core.handle());

        // Make sure our child succeeded in spawning
        let child = child.expect("failed to spawn");

        match core.run(child) {
            Ok(_) => println!("exited ok"),
            Err(e) => panic!("failed to wait for exit: {}", e),
        }
    }
}


fn appdown(c: &StopCommand) {
    println!("appdown!")
}


impl StopCommand {
    fn run(&self) {
        println!("stop_command"); }
}

impl StartCommand {
    fn run(&self) {
        loop {
            let mut core = Core::new().unwrap();

            // Use the standard library's `Command` type to build a process and
            // then execute it via the `CommandExt` trait.
            let mut new_c = Command::new(&self.command[0]);

            for i in &self.command[1..] {
                new_c.arg(i);
            }

            let child = new_c
                .current_dir(&Path::new(&self.cwd))
                .spawn_async(&core.handle());

            // Make sure our child succeeded in spawning
            let child = child.expect("failed to spawn");

            match core.run(child) {
                Ok(_) => println!("exited ok"),
                Err(e) => panic!("failed to wait for exit: {}", e),
            }
        }
    }
}

#[get("/")]
fn index() -> String {
    format!("Running commands:")
}

#[post("/command", format = "application/json", data = "<command>")]
fn command(command: Json<AppCommand>, rc: State<RunningCommands>) -> String {
    // TODO: there should be a smarter way using pattern match and
    // avoid invalid commands
    match command.state.as_ref() {
        "running" =>
            (StartCommand{command: command.command.clone() , cwd: command.cwd.clone().unwrap()}).run(),
        "stopped" =>
            StopCommand{command: command.command.clone()}.run(),
        _ => println!("baaad")
    }

    String::from("done")
}

fn init_api(){
    rocket::ignite()
        .manage(RunningCommands{ commands: HashMap::new() })
        .mount("/", routes![index, command])
        .launch();
}


fn main() {
    init_api()
}
